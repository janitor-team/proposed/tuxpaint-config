# This file is distributed under the same license as the Tux Paint package.
# Zdeněk Chalupský <chalzd@gmail.com>, 2010.
# 
msgid ""
msgstr ""
"Project-Id-Version: tuxpaint-config\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-14 00:38+0200\n"
"PO-Revision-Date: 2010-07-04 07:33+0100\n"
"Last-Translator: Zdeněk Chalupský <chalzd@gmail.com>\n"
"Language-Team: Czech <cs@li.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Poedit-Language: Czech\n"
"X-Poedit-Country: CZECH REPUBLIC\n"

#: ../about.h:10
msgid ""
"Welcome to Tux Paint's graphical configuration tool, created by Torsten "
"Giebl, Jan Wynholds, Bill Kendrick, and Martin Fuhrer.\n"
"\n"
"This tool allows you to alter Tux Paint's settings, such as full-screen "
"mode, the interface's language, or options to simplify Tux Paint for younger "
"children.\n"
"\n"
"These settings can be set for the current user only or for all users of your "
"computer by making a selection in the 'Settings for' pull-down menu at the "
"bottom.\n"
"\n"
"Use the tabs at the top to select the different setting categories, change "
"the options you'd like to change, and click the 'Apply' button at the bottom "
"to write out a new configuration file.\n"
"\n"
"The next time Tux Paint is launched, the new settings should take effect."
msgstr ""
"Víta vás nástroj pro nastavení grafikého programu Tux Paint, ktorý vytvořili "
"Torsten Giebl, Jan Wynholds, Bill Kendrick a Martin Fuhrer.\n"
"\n"
"Díky tomuto nástroji můžete změnit předvolené nastavení Tux Paintu, jako "
"například režim celé obrazovky, jazyk programu nebo nastavit zjednodušení "
"Tux Paintu pro menší děti.\n"
"\n"
"Tyto volby můžete nastavit buď pro právě přihlášeného uživatele, nebo pro "
"všechny uživatele počítače tak, že si vyberete jednu možnosť z nabídky "
"Nastavit pro.\n"
"\n"
"Kliknutím na záložky různobarevných karet si můžete zvolit jednotlivé "
"kategorie nastavení a změnit je, kliknutím na tlačítko Použít se změny uloží "
"do souboru s nastavením programu.\n"
"\n"
"Nové nastavení se projeví při novém spuštění programu Tux Paint."

#: ../tuxpaint-config.cxx:208
msgid "(Use system's default)"
msgstr "(Použít původní nastavení)"

#: ../tuxpaint-config2.cxx:92
msgid "(Use system's setting)"
msgstr "(Použít systémové nastavení)"

#: ../tuxpaint-config2.cxx:1661 ../tuxpaint-config2.cxx:2342
#, c-format
msgid "Default (no override)"
msgstr "Původní (nezměněná velikost)"

#: ../tuxpaint-config2.cxx:1663
#, fuzzy, c-format
#| msgid "Override: Small"
msgid "Override: Small (%d)"
msgstr "Změnit: Malé"

#: ../tuxpaint-config2.cxx:1665
#, fuzzy, c-format
#| msgid "Override: Medium"
msgid "Override: Medium (%d)"
msgstr "Změniť: Střední"

#: ../tuxpaint-config2.cxx:1667
#, fuzzy, c-format
#| msgid "Override: Large"
msgid "Override: Large (%d)"
msgstr "Změniť: Velké"

#: ../tuxpaint-config2.cxx:1684 ../tuxpaint-config2.cxx:2165
#, fuzzy, c-format
#| msgid "Default (no override)"
msgid "Button size: 48x48 (default; no override)"
msgstr "Původní (nezměněná velikost)"

#: ../tuxpaint-config2.cxx:1686
#, c-format
msgid "Button size: %dx%d"
msgstr ""

#: ../tuxpaint-config2.cxx:1703 ../tuxpaint-config2.cxx:2178
#, fuzzy, c-format
#| msgid "Default (no override)"
msgid "Color palette rows: 1 (default; no override)"
msgstr "Původní (nezměněná velikost)"

#: ../tuxpaint-config2.cxx:1705
#, fuzzy, c-format
#| msgid "Color Palette File:"
msgid "Color palette rows: %d"
msgstr "Soubor palety s barvami:"

#: ../tuxpaint-config2.cxx:1826
msgid "Save Directory?"
msgstr "Adresář pro ukládání?"

#: ../tuxpaint-config2.cxx:1843
#, fuzzy
#| msgid "Data Directory?"
msgid "Export Directory?"
msgstr "Adresář s údaji?"

#: ../tuxpaint-config2.cxx:1875
msgid "Data Directory?"
msgstr "Adresář s údaji?"

#: ../tuxpaint-config2.cxx:1906
msgid "Color Palette File?"
msgstr "Soubor palety s barvami?"

#: ../tuxpaint-config2.cxx:1945
msgid "Quit without applying changes?"
msgstr "Chceš skončit bez použití změn?"

#: ../tuxpaint-config2.cxx:1946
msgid "&No"
msgstr "&Ne"

#: ../tuxpaint-config2.cxx:1946
msgid "&Yes"
msgstr "An&o"

#: ../tuxpaint-config2.cxx:1982
msgid "current user"
msgstr "tohoto použivatele"

#: ../tuxpaint-config2.cxx:1985
msgid "all users"
msgstr "všechny uživatele"

#: ../tuxpaint-config2.cxx:1989
#, c-format
msgid "Apply changes you made to %s?"
msgstr "Mám použit změny, ktoré jste provedli v %s?"

#: ../tuxpaint-config2.cxx:1990
msgid "No"
msgstr "Ne"

#: ../tuxpaint-config2.cxx:1990
msgid "Yes"
msgstr "Ano"

#: ../tuxpaint-config2.cxx:2010 ../tuxpaint-config2.cxx:2013
msgid "Tux Paint Config v"
msgstr "Konfigurační nástroj programu Tux Paint, verze"

#. TAB: ABOUT
#: ../tuxpaint-config2.cxx:2022
msgid "About"
msgstr "O programu"

#: ../tuxpaint-config2.cxx:2029
msgid "About Tux Paint Config."
msgstr "O nástroji pro konfiguraci Tux Paintu"

#. TAB: VIDEO / SOUND
#: ../tuxpaint-config2.cxx:2045
#, fuzzy
#| msgid "Video / Sound"
msgid "Video/Sound"
msgstr "Video/Zvuk"

#: ../tuxpaint-config2.cxx:2052
msgid "Video:"
msgstr "Video:"

#: ../tuxpaint-config2.cxx:2056
msgid "&Fullscreen"
msgstr "Na &celou obrazovku"

#: ../tuxpaint-config2.cxx:2061
msgid "Run Tux Paint in fullscreen mode, rather than in a window."
msgstr "Tux Paint spouštět raději v režimu celé obrazovky."

#: ../tuxpaint-config2.cxx:2067
msgid "&Native"
msgstr "&Systémové"

#: ../tuxpaint-config2.cxx:2071
msgid "Use native screen resolution in fullscreen mode."
msgstr "V režimu celé obrazovky použít rozlišení monitoru."

#: ../tuxpaint-config2.cxx:2077
msgid "Window size : "
msgstr "Nastavte velikost okna : "

#: ../tuxpaint-config2.cxx:2101
msgid "Size of the window, or the resolution in fullscreen."
msgstr "V okně, nebo v režimu celé obrazovky."

#: ../tuxpaint-config2.cxx:2107
msgid "&Rotate Orientation"
msgstr "&Inteligentní otáčaní"

#: ../tuxpaint-config2.cxx:2111
msgid ""
"Swap width and height, to rotate 90 degrees (useful for portrait-display on "
"a tablet PC)"
msgstr ""
"Pri otáčaní o 90° prohodit  výšku a šířku (pomůže u tabletu při zobrazení na "
"výšku)"

#: ../tuxpaint-config2.cxx:2117
msgid "Allow S&creensaver"
msgstr "Povolit š&etřič obrazovky"

#: ../tuxpaint-config2.cxx:2121
msgid "Don't disable your system's screensaver while Tux Paint is running."
msgstr "Nevypínat šetřič na počítačí, pokud běží Tux Paint."

#: ../tuxpaint-config2.cxx:2130
msgid "Sound:"
msgstr "Zvuk:"

#: ../tuxpaint-config2.cxx:2134
msgid "Enable &Sound Effects"
msgstr "Zapnout &zvukové efekty"

#: ../tuxpaint-config2.cxx:2139
msgid "Enable/disable sound effects."
msgstr "Zapnout/vypnout zvukové efekty."

#: ../tuxpaint-config2.cxx:2144
#, fuzzy
#| msgid "Enable &Sound Effects"
msgid "Enable S&tereo Sound"
msgstr "Zapnout &zvukové efekty"

#: ../tuxpaint-config2.cxx:2149
msgid "Stereophonic ('stereo') or monaural ('mono') sound."
msgstr ""

#: ../tuxpaint-config2.cxx:2157
#, fuzzy
#| msgid "Interface Simplification:"
msgid "Interface Size:"
msgstr "Zjednodušení prostředí programu:"

#: ../tuxpaint-config2.cxx:2169
msgid ""
"(If button size is too large for the chosen window/screen resolution, the "
"largest usable size will be used.)"
msgstr ""

#. TAB: MOUSE / KEYBOARD
#: ../tuxpaint-config2.cxx:2188
#, fuzzy
#| msgid "Mouse / Keyboard"
msgid "Mouse/Keyboard"
msgstr "Myš/Klávesnice"

#: ../tuxpaint-config2.cxx:2196
msgid "Cursor:"
msgstr "Kurzor:"

#: ../tuxpaint-config2.cxx:2200
msgid "&Fancy Cursor Shapes"
msgstr "Používat různé tvary &kurzoru myši"

#: ../tuxpaint-config2.cxx:2205
msgid ""
"Change the shape of the mouse pointer depending on where the mouse is and "
"what you are doing."
msgstr "Měnit tvar ukazovatele myši podle toho, kde myš je a co děláme."

#: ../tuxpaint-config2.cxx:2211
msgid "&Hide Cursor"
msgstr "&Skrýt kurzor"

#: ../tuxpaint-config2.cxx:2216
msgid "Completely hides cursor (useful on touchscreen devices)."
msgstr "Úplně skryje kurzor (užitečné na dotykových zařízeních)."

#. Fl_Group* o
#: ../tuxpaint-config2.cxx:2225 ../tuxpaint-config2.cxx:2902
msgid "Keyboard:"
msgstr "Klávesnice:"

#: ../tuxpaint-config2.cxx:2229
msgid "Enable &Keyboard Shortcuts"
msgstr "Povolit &klávesové zkratky"

#: ../tuxpaint-config2.cxx:2234
msgid ""
"Allows key combinations to be used as shortcuts for certain commands. (e.g., "
"Control+S to Save)"
msgstr ""
"Umožní používat kombinace kláves jako příkazů pro některou činnost, (např. "
"současné stisknutí Ctrl a S pro uložení)"

#: ../tuxpaint-config2.cxx:2246 ../tuxpaint-config2.cxx:2887
msgid "Mouse:"
msgstr "Myš:"

#: ../tuxpaint-config2.cxx:2250
msgid "&Grab Mouse Pointer"
msgstr "&Kontrolovat ukazatel myši"

#: ../tuxpaint-config2.cxx:2255
msgid "Prevents the mouse pointer from leaving the Tux Paint window."
msgstr "Zabráníte, aby ukazatel myši opustil okno programu Tux Paint."

#: ../tuxpaint-config2.cxx:2260
msgid "Mouse &Wheel Support"
msgstr "Podpora &kolečka myši"

#: ../tuxpaint-config2.cxx:2265
msgid ""
"Allows a mouse wheel to be used to scroll through items. (e.g., brushes, "
"stamps)"
msgstr ""
"Umožní, aby jste mohli vybírat věci pomocí kolečka myši (např. štětce nebo "
"razítka)"

#: ../tuxpaint-config2.cxx:2270
msgid "No &Button Distinction"
msgstr "&Nerozlišovat tlačítka"

#: ../tuxpaint-config2.cxx:2274
msgid "Allows middle and right mouse buttons to be used for clicking, too."
msgstr "Umožní, aby jste mohli klikat i středním a pravým tlačítkem myši."

#. TAB: SIMPLIFCIATION
#. FIXME: From here on, not using 'boxx/boxy' trick, used above -bjk 2011.04.15
#: ../tuxpaint-config2.cxx:2287
msgid "Simplification"
msgstr "Zjednodušení"

#: ../tuxpaint-config2.cxx:2295
msgid "Interface Simplification:"
msgstr "Zjednodušení prostředí programu:"

#: ../tuxpaint-config2.cxx:2299
msgid "Disable Shape &Rotation"
msgstr "V&ypnout otáčaní geometrických útvarů"

#: ../tuxpaint-config2.cxx:2304
msgid "Shape tool's rotation step is disabled. Useful for younger children."
msgstr "Vypne sa nástroj na otáčaní. Užitečné pre menší děti."

#: ../tuxpaint-config2.cxx:2309
msgid "Simple Stamp &Outlines"
msgstr "&Jednoduchá razítka"

#: ../tuxpaint-config2.cxx:2314
msgid ""
"Draw a rectangle around the cursor when placing stamps, rather than a "
"detailed outline. (For slow computers and thin clients.)"
msgstr ""
"Kreslit jen obdélník bez obrázku (pro pomalé počítače a klienty větších "
"sítí)."

#: ../tuxpaint-config2.cxx:2319
msgid "Show &Uppercase Text Only"
msgstr "&Zobrazit pouze velké písmena"

#: ../tuxpaint-config2.cxx:2324
msgid ""
"Cause all text in Tux Paint (button labels, dialogs, etc.) to appear in "
"UPPERCASE rather than Mixed Case."
msgstr ""
"Příkaz způsobí, že veškerý text v Tux Paintu (názvy na tlačítkách, oznámení, "
"atd.) budou napsané pouze VELKÝMI PÍSMENY, a ne Růzým druhem písma."

#: ../tuxpaint-config2.cxx:2333
msgid "Initial Stamp Size:"
msgstr "Předvolená velikost razítka:"

#: ../tuxpaint-config2.cxx:2350
msgid "Control Simplification:"
msgstr "Zjednodušení tlačítek:"

#: ../tuxpaint-config2.cxx:2354
msgid "Disable '&Quit' Button and [Escape] key"
msgstr "Vypnout tlačítko \"Konec\" a zablokovat Esc"

#: ../tuxpaint-config2.cxx:2359
msgid ""
"Clicking the window's close (X) button in the title bar, or pressing "
"[Alt]+[F4] or [Shift]+[Ctrl]+[Escape] will still quit Tux Paint."
msgstr ""
"Kliknutím na (X) na horním panelu, nebo současným stiskem kláves Alt + F4 "
"nebo Shift + Ctrl + Escape, Tux Paint ukončíte."

#: ../tuxpaint-config2.cxx:2364
msgid "Disable '&Stamps' Tool"
msgstr "Vypnout nástroj &Razítka"

#: ../tuxpaint-config2.cxx:2369
msgid "Do not load stamps at startup, thus disabling the Stamps tool."
msgstr ""
"Nezobrazovat tlačítko razítka pri startu programu, a tak znemožnt jejich "
"používání."

#: ../tuxpaint-config2.cxx:2374
msgid "Disable Stamp &Controls"
msgstr "Vypnout tlačítka na úpr&avu razítek"

#: ../tuxpaint-config2.cxx:2379
msgid ""
"Simplify the 'Stamps' tool by removing the stamp control buttons (Shrink, "
"Grow, Mirror and Flip)."
msgstr ""
"Zjednodušit razítka odebráním tlačítek pro jejich úpravu (zmenšení, "
"zvětšování, zrcadlový obraz a převrácení)."

#: ../tuxpaint-config2.cxx:2384
#, fuzzy
#| msgid "Disable Stamp &Controls"
msgid "Disable &Magic Controls"
msgstr "Vypnout tlačítka na úpr&avu razítek"

#: ../tuxpaint-config2.cxx:2389
msgid ""
"Simplify the 'Magic' tools by removing the buttons to switch between paint "
"and fullscreen modes."
msgstr ""
"Zjednoduš Magické nástroje odebráním tlačítek pro přepínání režimů kreslení, "
"výběrem a celoobrazovkový režim."

#: ../tuxpaint-config2.cxx:2394
#, fuzzy
#| msgid "Disable Stamp &Controls"
msgid "Disable Shape Controls"
msgstr "Vypnout tlačítka na úpr&avu razítek"

#: ../tuxpaint-config2.cxx:2398
#, fuzzy
#| msgid ""
#| "Simplify the 'Magic' tools by removing the buttons to switch between "
#| "paint and fullscreen modes."
msgid ""
"Simplify the 'Shapes' tool by removing the buttons to switch between center- "
"and corner-based drawing."
msgstr ""
"Zjednoduš Magické nástroje odebráním tlačítek pro přepínání režimů kreslení, "
"výběrem a celoobrazovkový režim."

#: ../tuxpaint-config2.cxx:2403
msgid "Disable '&Label' Tool"
msgstr "Vypnout nástroj &Razítka"

#: ../tuxpaint-config2.cxx:2408
#, fuzzy
msgid "Disable the 'Label' text-entry tool (leaving only the 'Text' tool)."
msgstr ""
"Zakázat  'Label' text-entry nástroj (leaving only the 'Text' nástrojl)."

#. TAB: LANGUAGES
#: ../tuxpaint-config2.cxx:2419
msgid "Languages"
msgstr "Jazyky"

#: ../tuxpaint-config2.cxx:2425
msgid "Language:"
msgstr "Jazyk:"

#: ../tuxpaint-config2.cxx:2429
msgid "Language : "
msgstr "Jazyky: "

#: ../tuxpaint-config2.cxx:2444
msgid "Run Tux Paint in a particular language (overriding system's settings)."
msgstr ""
"Spustí Tux Paint v určitém jazyce (podle systémových nastavení počítače)."

#: ../tuxpaint-config2.cxx:2449
msgid "&Mirror Stamps"
msgstr "&Zrcadlová razítka"

#: ../tuxpaint-config2.cxx:2454
msgid ""
"Automatically mirror-image all mirror-able stamps. Useful for users who "
"prefer things appearing right-to-left."
msgstr ""
"Automaticky vytvoří zrcadlový obraz všech razítek, u ktorých je to možné. Je "
"to užitečné pro ty, kteří se na věci raději dívají zprava doleva."

#: ../tuxpaint-config2.cxx:2463
msgid "Fonts:"
msgstr "Písma:"

#: ../tuxpaint-config2.cxx:2467
msgid "Load System &Fonts"
msgstr "Nahrát &systémové písmo"

#: ../tuxpaint-config2.cxx:2472
msgid ""
"Attempt to load more fonts, found elsewhere on your computer. (Note: may "
"cause instability!)"
msgstr ""
"Program sa pokusil načíst více písem, které jsou uložené v počítači. "
"(Upozornění: může to způsobit nestabilitu programu!)"

#: ../tuxpaint-config2.cxx:2477
msgid "Load All &Locale Fonts"
msgstr "Načíst písma pro všechny &jazyky"

#: ../tuxpaint-config2.cxx:2482
msgid ""
"Load all locale-specific fonts installed in Tux Paint, regardless of the "
"locale Tux Paint is being run under."
msgstr ""
"Načíst všechny písma charakteristické pro určitý jazyk, nainstalované v Tux "
"Painte, bez ohledu na jazyk programu."

#. TAB: PRINTING
#: ../tuxpaint-config2.cxx:2493
msgid "Printing"
msgstr "Tisk"

#: ../tuxpaint-config2.cxx:2499
msgid "Print Permissions:"
msgstr "Oprávnění k tisku:"

#: ../tuxpaint-config2.cxx:2503
msgid "Allow &Printing"
msgstr "&Povolit tisk"

#: ../tuxpaint-config2.cxx:2508
msgid "Let users print from within Tux Paint."
msgstr "Povolit uživatelům tisknout v Tux Paintu."

#: ../tuxpaint-config2.cxx:2513
msgid "Print Delay : "
msgstr "Odložení tisku: "

#: ../tuxpaint-config2.cxx:2519
msgid "seconds"
msgstr "sekund (vteřin)"

#: ../tuxpaint-config2.cxx:2523
msgid ""
"Restrict printing to once every N seconds. (Enter '0' to allow unrestricted "
"printing.)"
msgstr ""
"Můžete omezit tisk na jeden za N sekund. (Pokud tisk nechcete omezovat, "
"napište nulu.)"

#: ../tuxpaint-config2.cxx:2531
msgid "Show Printer Dialog:"
msgstr "Zobrazit informáce o tisku:"

#: ../tuxpaint-config2.cxx:2536
msgid "Only when [Alt] &modifier key is held"
msgstr "Pouze když je &tlačítko Alt stisknuté"

#: ../tuxpaint-config2.cxx:2543
msgid "Always &show printer dialog"
msgstr "Údaje o tisku zobrazit &vždy"

#: ../tuxpaint-config2.cxx:2550
msgid "&Never show printer dialog"
msgstr "&Nikdy nezobrazovat údaje o tisku"

#: ../tuxpaint-config2.cxx:2557
msgid "(Even when [Alt] is held.)"
msgstr "(I když je klávesa Alt stisknutá.)"

#: ../tuxpaint-config2.cxx:2571
msgid "Save printer configuration"
msgstr "Uložiť nastavení pro tisk"

#: ../tuxpaint-config2.cxx:2580
msgid "Print Commands:"
msgstr "Příkazy pro tisk:"

#: ../tuxpaint-config2.cxx:2584
msgid "Use &Alternative Print Command"
msgstr "Používat &náhradní příkazy pro tisk"

#: ../tuxpaint-config2.cxx:2589
msgid ""
"Override Tux Paint's default setting for print command ('lpr') with another. "
"(Advanced! Unix/Linux only!)"
msgstr "Nahradi původní nastavení programu Tux Paint pro příkaz (lpr) za jiný."

#: ../tuxpaint-config2.cxx:2599
msgid ""
"Enter the command for printing. It must accept a PostScript format on its "
"standard input (STDIN)."
msgstr ""
"Zadajte příkaz pro tiskč. Na standardním vstupu (STDIN) třeba povolit "
"PostScript."

#: ../tuxpaint-config2.cxx:2610
msgid "Use &Alternative Print Dialog"
msgstr "Používat &alternativní údaje o tisku"

#: ../tuxpaint-config2.cxx:2615
msgid ""
"Override Tux Paint's default setting for print dialog ('kprinter') with "
"another. (Advanced! Unix/Linux only!)"
msgstr ""
"Přepsat nastavení pro naprogramované informace o tisku ('kprinter'). (Pro "
"pokročilé uživatele! Pouze v operačním systému Unix/Linux!)"

#: ../tuxpaint-config2.cxx:2625
msgid ""
"Enter the print dialog command. It must accept a PostScript format on its "
"standard input (STDIN)."
msgstr ""
"Napište příkaz pro tisk. Musí v něm být zahrnutý i typ písma PostScript pro "
"standardní vstup (STDIN)."

#: ../tuxpaint-config2.cxx:2632
msgid "Paper Size : "
msgstr "Velikost papíru: "

#. TAB: SAVING
#: ../tuxpaint-config2.cxx:2653
msgid "Saving"
msgstr "Ukládání"

#: ../tuxpaint-config2.cxx:2660
msgid "Save Over Earlier Work:"
msgstr "Změnit původní soubor:"

#: ../tuxpaint-config2.cxx:2664
msgid "&Ask Before Overwriting"
msgstr "&Zeptat se před jeho přepsáním"

#: ../tuxpaint-config2.cxx:2669
msgid ""
"When re-saving an image, ask whether to overwrite the earlier version, or "
"make a new file."
msgstr ""
"Program se vždy zeptá, zda chcete změnit původní soubor, nebo chcete změnu "
"uložit do nového souboru."

#: ../tuxpaint-config2.cxx:2674
msgid "Always &Overwrite Older Version"
msgstr "Vždy &přepsat původní soubor"

#: ../tuxpaint-config2.cxx:2679
msgid ""
"When re-saving an image, always overwrite the earlier version. (Warning: "
"Potential for lost work!)"
msgstr ""
"Změny se automaticky uloží do existujícího souboru. (Upozornění: Můžete "
"přijít o svou práci!)"

#: ../tuxpaint-config2.cxx:2684
msgid "Always Save &New Picture"
msgstr "Vždy uložit do &nového souboru."

#: ../tuxpaint-config2.cxx:2689
msgid ""
"When re-saving an image, always make a new file. (Warning: Potential for "
"lots of files!)"
msgstr ""
"Změny se uloží vždy do nového souboru. (Pozor na velké množství souborů.)"

#: ../tuxpaint-config2.cxx:2696
msgid "Starting Out:"
msgstr ""

#: ../tuxpaint-config2.cxx:2701
msgid "Start with &Blank Canvas"
msgstr "Začít s č&istým plátnem"

#. Place solid color backgrounds at the end of the "New" dialog, promoting pre-drawn Starters and Templates to the top
#: ../tuxpaint-config2.cxx:2708
msgid "Colors last in 'New' dialog"
msgstr ""

#: ../tuxpaint-config2.cxx:2717
#, fuzzy
#| msgid "Save Directory:"
msgid "Save and Export Directories:"
msgstr "Adresář pro ukládání obrázků:"

#. Save directory
#: ../tuxpaint-config2.cxx:2722
msgid "Use &Alternative Save Directory"
msgstr "Obrázky ukládat do &náhradního adresáře"

#: ../tuxpaint-config2.cxx:2727
msgid ""
"Do not save pictures in the standard directory, use the following location:"
msgstr "Neukládat obrázky do nastaveného adresáře, ale použít tento:"

#: ../tuxpaint-config2.cxx:2732
msgid "Alternative Save Directory:"
msgstr "Náhradní adresář pro obrázky:"

#: ../tuxpaint-config2.cxx:2737 ../tuxpaint-config2.cxx:2757
#: ../tuxpaint-config2.cxx:2836 ../tuxpaint-config2.cxx:2864
msgid "Browse..."
msgstr "Prohledat..."

#. Export directory
#: ../tuxpaint-config2.cxx:2743
#, fuzzy
#| msgid "Use &Alternative Data Directory"
msgid "Use Alternative Export Directory"
msgstr "Používat &náhradní adresář s údaji"

#: ../tuxpaint-config2.cxx:2747
#, fuzzy
#| msgid ""
#| "Do not save pictures in the standard directory, use the following "
#| "location:"
msgid ""
"Do not export pictures and animated GIFs in the standard directory, use the "
"following location:"
msgstr "Neukládat obrázky do nastaveného adresáře, ale použít tento:"

#: ../tuxpaint-config2.cxx:2752
#, fuzzy
#| msgid "Alternative Data Directory:"
msgid "Alternative Export Directory:"
msgstr "Náhradní adresář pro údaje:"

#: ../tuxpaint-config2.cxx:2763
msgid "More Saving Options:"
msgstr "Více možností pro uložení:"

#: ../tuxpaint-config2.cxx:2768
msgid "Disable '&Save' Button"
msgstr "Zakázat tlačítko &Uložit"

#: ../tuxpaint-config2.cxx:2774
msgid "&Auto-save on Quit"
msgstr "Při ukončení programu uložit &automaticky"

#: ../tuxpaint-config2.cxx:2779
msgid "Don't ask to save current picture when quitting; just save."
msgstr "Pri skončení se neptat, zda se má obrázek uložit, pouze ho uložit."

#. TAB: DATA
#: ../tuxpaint-config2.cxx:2791
msgid "Data"
msgstr "Údaje"

#. FIXME: Looks awful:
#: ../tuxpaint-config2.cxx:2799
msgid "Lockfile:"
msgstr "Soubor so zámkem:"

#: ../tuxpaint-config2.cxx:2803
msgid "&Don't use lockfile"
msgstr "&Nepoužívat soubor na zamykání"

#: ../tuxpaint-config2.cxx:2807
msgid ""
"Do not check for a lockfile. Allow Tux Paint to be launched multiple times. "
"(May be necessary in a networked environment.)"
msgstr ""
"Nehledat soubor se zámkem. Povolit, Tux Paint spustit vícekrát. (Může to být "
"potřebné při provozu na síti.)"

#: ../tuxpaint-config2.cxx:2817
msgid "Data Directory:"
msgstr "Adresář pro údaje:"

#: ../tuxpaint-config2.cxx:2821
msgid "Use &Alternative Data Directory"
msgstr "Používat &náhradní adresář s údaji"

#: ../tuxpaint-config2.cxx:2826
msgid ""
"Do not load brushes, stamps, etc. from the standard directory, use the "
"following location:"
msgstr ""
"Nespouštět štětce, razítka, atd. z nastaveného adresáře, ale používat tento:"

#: ../tuxpaint-config2.cxx:2831
msgid "Alternative Data Directory:"
msgstr "Náhradní adresář pro údaje:"

#: ../tuxpaint-config2.cxx:2845 ../tuxpaint-config2.cxx:2859
msgid "Color Palette File:"
msgstr "Soubor palety s barvami:"

#: ../tuxpaint-config2.cxx:2849
msgid "Use &Alternative Color Palette"
msgstr "Použít j&inou paletu s barvami"

#: ../tuxpaint-config2.cxx:2854
msgid ""
"Don't use default color palette, use colors defined in the following file:"
msgstr "Nepoužívat původní paletu, ale barvy definované v tomto souboru:"

#: ../tuxpaint-config2.cxx:2881
msgid "Accessibility"
msgstr ""

#: ../tuxpaint-config2.cxx:2891
msgid "Sticky mouse clicks"
msgstr ""

#. Fl_Check_Button* o
#: ../tuxpaint-config2.cxx:2895
msgid ""
"Useful for users who have difficulty clicking and dragging. When enabled, "
"click and release to start painting, move to paint, and click and release "
"again to stop. It can also be combined with joystick- and keyboard-based "
"pointer controls."
msgstr ""

#: ../tuxpaint-config2.cxx:2906
msgid "Keyboard controls the mouse pointer"
msgstr ""

#. Fl_Check_Button* o
#: ../tuxpaint-config2.cxx:2910
msgid ""
"When enabled, the arrow keys or numbers can be used to move the mouse "
"pointer. Number 5, Space, Insert or F5 can be used to click. (When in \"Text"
"\" or \"Label\" tools, the numbers and space cannot be used.) Also, F4 "
"cycles the pointer between the \"Tools\" box, \"Colors\" box and drawing "
"canvas, F8 and F7 move up/down inside the \"Tools\" box, and F11 and F12 "
"move left/right inside the \"Tools\" and \"Colors\" boxes."
msgstr ""

#: ../tuxpaint-config2.cxx:2918
msgid "Onscreen keyboard:"
msgstr ""

#: ../tuxpaint-config2.cxx:2922
msgid "Show a keyboard on the screen"
msgstr ""

#. Fl_Check_Button* o
#: ../tuxpaint-config2.cxx:2926
msgid ""
"Display a keyboard on the screen when the \"Text\" and \"Label\" tools are "
"enabled, so you can 'type' with the mouse pointer."
msgstr ""

#. Fl_Box* o
#: ../tuxpaint-config2.cxx:2931
msgid "Layout"
msgstr ""

#. Fl_Choice* o
#: ../tuxpaint-config2.cxx:2943
msgid "How are keys organized in the keyboard"
msgstr ""

#. Fl_Box* o
#: ../tuxpaint-config2.cxx:2948
msgid "Disable layout changes"
msgstr ""

#. Fl_Check_Button* o
#: ../tuxpaint-config2.cxx:2953
msgid "Disable the buttons that allow changing the keyboard layout."
msgstr ""

#. Fl_Group* o
#. TAB: JOYSTICK
#: ../tuxpaint-config2.cxx:2965
msgid "Joystick"
msgstr ""

#. o->box(FL_PLASTIC_UP_BOX);
#: ../tuxpaint-config2.cxx:2976
msgid "Main device:"
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:2986
msgid "Device number to use. (SDL starts numbering at 0)"
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:2997
msgid ""
"Joystick slowness. Increase this value for people with slow reactions. "
"(0-500; default value is 15)"
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3009
msgid ""
"SDL events under threshold will be discarded, useful to discard accidental "
"unwanted movements. (0-32766; default value is 3200)"
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3019
msgid "Limit speed when moving. (1-7; default value is 7)"
msgstr ""

#. Fl_Group* o
#: ../tuxpaint-config2.cxx:3026
msgid "Hat:"
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3035
msgid "Hat slowness. (0-500; default value is 15)"
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3047
msgid ""
"Hat timeout, the number of milliseconds to wait before starting to move "
"continuously. (0-3000; default value is 1000)"
msgstr ""

#. Fl_Group* o
#: ../tuxpaint-config2.cxx:3058
msgid "Buttons to disable:"
msgstr ""

#. Fl_Input* o
#: ../tuxpaint-config2.cxx:3074
msgid ""
"If for any reason one or more buttons of the joystick are posing problems, "
"you can disable them here using a comma separated list of button numbers. (e."
"g. 2,3,5)"
msgstr ""

#: ../tuxpaint-config2.cxx:3083
msgid "Button shortcuts:"
msgstr ""

#: ../tuxpaint-config2.cxx:3087
msgid ""
"Here you can configure shortcuts for the different buttons of the joystick. "
"(Beware to not assign a shortcut to the button used to draw.)"
msgstr ""

#. Fl_Box* o
#: ../tuxpaint-config2.cxx:3092
msgid "Button number for the Escape key."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3100
msgid "Button number for the Brush tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3108
msgid "Button number for the Stamps tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3116
msgid "Button number for the Lines tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3124
msgid "Button number for the Shapes tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3132
msgid "Button number for the Text tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3140
msgid "Button number for the Label tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3148
msgid "Button number for the Magic tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3156
msgid "Button number for Undo."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3164
msgid "Button number for Redo."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3172
msgid "Button number for the Eraser tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3180
msgid "Button number for the New tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3188
msgid "Button number for the Open tool."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3196
msgid "Button number for saving."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3204
msgid "Button number for Page Setup."
msgstr ""

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3212
msgid "Button number for printing."
msgstr ""

#: ../tuxpaint-config2.cxx:3234
msgid "Settings for : "
msgstr "Nastavit pro : "

#: ../tuxpaint-config2.cxx:3238
msgid "Current User"
msgstr "mne"

#: ../tuxpaint-config2.cxx:3239
msgid "All Users"
msgstr "všechny"

#: ../tuxpaint-config2.cxx:3245
msgid "Use \"All Users\" Settings"
msgstr "Použít pro všechny"

#: ../tuxpaint-config2.cxx:3249
msgid "Apply"
msgstr "Použít"

#: ../tuxpaint-config2.cxx:3257
msgid "Reset"
msgstr "Předchozí"

#: ../tuxpaint-config2.cxx:3264
msgid "Defaults"
msgstr "Výchozí"

#: ../tuxpaint-config2.cxx:3271
msgid "Quit"
msgstr "Konec"

#. vim:ts=8:sts=3
#: ../tuxpaint-config.desktop.in.h:1
msgid "Tux Paint Config."
msgstr "Nástroj na konfiguraci Tux Paintu."

#: ../tuxpaint-config.desktop.in.h:2
msgid "Configure Tux Paint"
msgstr "Nastavit Tux Paint"

#~ msgid ""
#~ "Don't allow pictures to be saved. Tux Paint acts as temporary 'scratch "
#~ "paper.'"
#~ msgstr ""
#~ "Nedovolit obrázky uložit. Tux Paint slouží pouze jako šmírák - cvičná "
#~ "plocha."

#~ msgid "Start Blank:"
#~ msgstr "Začít s prázdnou stránkou:"

#~| msgid "Disable Magic &Controls"
#~ msgid "Zakázat tlačítka &Magic"
#~ msgstr "Zakázat tlačítka M&agic"
