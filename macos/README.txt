PREREQUISITES
-------------
First, install XCode, MacPorts, and its dependencies required for building Tux
Paint.  These are described in the macos/README.txt file in the Tux Paint
source code.

Additionally, the following MacPorts package is required:

  fltk-devel


HOW TO BUILD
------------
Simply, run

  % make macos
  % make install-macos


KNOWN ISSUES
------------
- fltk-devel currently (@20201213-d8635887_0) has an issue where it attempts to
  use macOS 11.0 SDK even on macOS 11.1 (both versions of macOS which are
  confusingly known as 'Big Sur'.)  To get around this issue, modify
  /opt/local/bin/fltk-config to change references to MacOSX11.0.sdk with
  MacOSX11.1.sdk.

  A ticket has been filed with Mac Ports to address this issue:

    https://trac.macports.org/ticket/62018

